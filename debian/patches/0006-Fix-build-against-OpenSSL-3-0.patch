From: Nicholas Guriev <guriev-ns@ya.ru>
Date: Tue, 31 May 2022 22:31:08 +0300
Subject: Make build against OpenSSL-3.0 possible
 In OpenSSL, the SSL_get_ex_new_index macro (substituted to
 CRYPTO_get_ex_new_index) requires CRYPTO_EX_dup as the second callback. This
 typedef, for some reason, has got an extra asterisk near void* within
 arguments into the third version. Freely conversions from void* to void** is
 okay in C but prohibited in C++. So I've updated the callback prototype to
 match the last OpenSSL version.
 .
 OpenSSL pre-3.0 defined all of the SSL_OP_* macros with numeric hexadecimal
 literals. However, the third version uses there casting expressions with
 shifts which preprocessor is unable to compute. So I check only macros
 existence, this lets Squid accept obsolete options. But it's nothing,
 OpenSSL should ignore them anyway.

---
 acinclude/lib-checks.m4     |    2 -
 src/security/PeerOptions.cc |   50 ++++++++++++++++++++++----------------------
 src/ssl/support.cc          |    2 -
 3 files changed, 27 insertions(+), 27 deletions(-)

--- a/acinclude/lib-checks.m4
+++ b/acinclude/lib-checks.m4
@@ -236,7 +236,7 @@ AC_DEFUN([SQUID_CHECK_OPENSSL_CONST_CRYP
   AC_COMPILE_IFELSE([AC_LANG_PROGRAM([
 #include <openssl/ssl.h>
 
-int const_dup_func(CRYPTO_EX_DATA *, const CRYPTO_EX_DATA *, void *, int, long, void *) {
+int const_dup_func(CRYPTO_EX_DATA *, const CRYPTO_EX_DATA *, void **, int, long, void *) {
     return 0;
 }
     ],[
--- a/src/security/PeerOptions.cc
+++ b/src/security/PeerOptions.cc
@@ -297,130 +297,130 @@ static struct ssl_option {
 
 } ssl_options[] = {
 
-#if SSL_OP_NETSCAPE_REUSE_CIPHER_CHANGE_BUG
+#ifdef SSL_OP_NETSCAPE_REUSE_CIPHER_CHANGE_BUG
     {
         "NETSCAPE_REUSE_CIPHER_CHANGE_BUG", SSL_OP_NETSCAPE_REUSE_CIPHER_CHANGE_BUG
     },
 #endif
-#if SSL_OP_SSLREF2_REUSE_CERT_TYPE_BUG
+#ifdef SSL_OP_SSLREF2_REUSE_CERT_TYPE_BUG
     {
         "SSLREF2_REUSE_CERT_TYPE_BUG", SSL_OP_SSLREF2_REUSE_CERT_TYPE_BUG
     },
 #endif
-#if SSL_OP_MICROSOFT_BIG_SSLV3_BUFFER
+#ifdef SSL_OP_MICROSOFT_BIG_SSLV3_BUFFER
     {
         "MICROSOFT_BIG_SSLV3_BUFFER", SSL_OP_MICROSOFT_BIG_SSLV3_BUFFER
     },
 #endif
-#if SSL_OP_SSLEAY_080_CLIENT_DH_BUG
+#ifdef SSL_OP_SSLEAY_080_CLIENT_DH_BUG
     {
         "SSLEAY_080_CLIENT_DH_BUG", SSL_OP_SSLEAY_080_CLIENT_DH_BUG
     },
 #endif
-#if SSL_OP_TLS_D5_BUG
+#ifdef SSL_OP_TLS_D5_BUG
     {
         "TLS_D5_BUG", SSL_OP_TLS_D5_BUG
     },
 #endif
-#if SSL_OP_TLS_BLOCK_PADDING_BUG
+#ifdef SSL_OP_TLS_BLOCK_PADDING_BUG
     {
         "TLS_BLOCK_PADDING_BUG", SSL_OP_TLS_BLOCK_PADDING_BUG
     },
 #endif
-#if SSL_OP_TLS_ROLLBACK_BUG
+#ifdef SSL_OP_TLS_ROLLBACK_BUG
     {
         "TLS_ROLLBACK_BUG", SSL_OP_TLS_ROLLBACK_BUG
     },
 #endif
-#if SSL_OP_ALL
+#ifdef SSL_OP_ALL
     {
         "ALL", (long)SSL_OP_ALL
     },
 #endif
-#if SSL_OP_SINGLE_DH_USE
+#ifdef SSL_OP_SINGLE_DH_USE
     {
         "SINGLE_DH_USE", SSL_OP_SINGLE_DH_USE
     },
 #endif
-#if SSL_OP_EPHEMERAL_RSA
+#ifdef SSL_OP_EPHEMERAL_RSA
     {
         "EPHEMERAL_RSA", SSL_OP_EPHEMERAL_RSA
     },
 #endif
-#if SSL_OP_PKCS1_CHECK_1
+#ifdef SSL_OP_PKCS1_CHECK_1
     {
         "PKCS1_CHECK_1", SSL_OP_PKCS1_CHECK_1
     },
 #endif
-#if SSL_OP_PKCS1_CHECK_2
+#ifdef SSL_OP_PKCS1_CHECK_2
     {
         "PKCS1_CHECK_2", SSL_OP_PKCS1_CHECK_2
     },
 #endif
-#if SSL_OP_NETSCAPE_CA_DN_BUG
+#ifdef SSL_OP_NETSCAPE_CA_DN_BUG
     {
         "NETSCAPE_CA_DN_BUG", SSL_OP_NETSCAPE_CA_DN_BUG
     },
 #endif
-#if SSL_OP_NON_EXPORT_FIRST
+#ifdef SSL_OP_NON_EXPORT_FIRST
     {
         "NON_EXPORT_FIRST", SSL_OP_NON_EXPORT_FIRST
     },
 #endif
-#if SSL_OP_CIPHER_SERVER_PREFERENCE
+#ifdef SSL_OP_CIPHER_SERVER_PREFERENCE
     {
         "CIPHER_SERVER_PREFERENCE", SSL_OP_CIPHER_SERVER_PREFERENCE
     },
 #endif
-#if SSL_OP_NETSCAPE_DEMO_CIPHER_CHANGE_BUG
+#ifdef SSL_OP_NETSCAPE_DEMO_CIPHER_CHANGE_BUG
     {
         "NETSCAPE_DEMO_CIPHER_CHANGE_BUG", SSL_OP_NETSCAPE_DEMO_CIPHER_CHANGE_BUG
     },
 #endif
-#if SSL_OP_NO_SSLv3
+#ifdef SSL_OP_NO_SSLv3
     {
         "NO_SSLv3", SSL_OP_NO_SSLv3
     },
 #endif
-#if SSL_OP_NO_TLSv1
+#ifdef SSL_OP_NO_TLSv1
     {
         "NO_TLSv1", SSL_OP_NO_TLSv1
     },
 #else
     { "NO_TLSv1", 0 },
 #endif
-#if SSL_OP_NO_TLSv1_1
+#ifdef SSL_OP_NO_TLSv1_1
     {
         "NO_TLSv1_1", SSL_OP_NO_TLSv1_1
     },
 #else
     { "NO_TLSv1_1", 0 },
 #endif
-#if SSL_OP_NO_TLSv1_2
+#ifdef SSL_OP_NO_TLSv1_2
     {
         "NO_TLSv1_2", SSL_OP_NO_TLSv1_2
     },
 #else
     { "NO_TLSv1_2", 0 },
 #endif
-#if SSL_OP_NO_TLSv1_3
+#ifdef SSL_OP_NO_TLSv1_3
     {
         "NO_TLSv1_3", SSL_OP_NO_TLSv1_3
     },
 #else
     { "NO_TLSv1_3", 0 },
 #endif
-#if SSL_OP_NO_COMPRESSION
+#ifdef SSL_OP_NO_COMPRESSION
     {
         "No_Compression", SSL_OP_NO_COMPRESSION
     },
 #endif
-#if SSL_OP_NO_TICKET
+#ifdef SSL_OP_NO_TICKET
     {
         "NO_TICKET", SSL_OP_NO_TICKET
     },
 #endif
-#if SSL_OP_SINGLE_ECDH_USE
+#ifdef SSL_OP_SINGLE_ECDH_USE
     {
         "SINGLE_ECDH_USE", SSL_OP_SINGLE_ECDH_USE
     },
@@ -512,7 +512,7 @@ Security::PeerOptions::parseOptions()
 
     }
 
-#if SSL_OP_NO_SSLv2
+#ifdef SSL_OP_NO_SSLv2
     // compliance with RFC 6176: Prohibiting Secure Sockets Layer (SSL) Version 2.0
     op = op | SSL_OP_NO_SSLv2;
 #endif
--- a/src/ssl/support.cc
+++ b/src/ssl/support.cc
@@ -559,7 +559,7 @@ Ssl::VerifyCallbackParameters::At(Securi
 // "dup" function for SSL_get_ex_new_index("cert_err_check")
 #if SQUID_USE_CONST_CRYPTO_EX_DATA_DUP
 static int
-ssl_dupAclChecklist(CRYPTO_EX_DATA *, const CRYPTO_EX_DATA *, void *,
+ssl_dupAclChecklist(CRYPTO_EX_DATA *, const CRYPTO_EX_DATA *, void **,
                     int, long, void *)
 #else
 static int
